source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

ruby '2.6.1'

# framework
gem 'rails', '~> 5.2.3'

# db
gem 'mysql2', '>= 0.3.18', '< 0.6.0'

# config
gem('config')

# rack
gem 'unicorn'
gem 'unicorn-worker-killer' # unicornのプロセスをメモリの使用量に応じて再起動する

# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby
gem 'sqlite3'

# react
gem 'react-rails'
gem 'webpacker'


group :local, :development, :test do
  # automation
  gem 'listen', '>= 3.0.5', '< 3.2'

  # benchmark
  gem 'bullet' # N+1問題を検出してくれるgem

  # comment
  gem 'yard' # ドキュメント生成

  # data
  gem 'faker' # ダミーデータ生成
  gem 'seed_dump' # DBからseedを作成可能とする

  # debug
  gem 'byebug', platforms: %i[mri mingw x64_mingw] # デバッガ
  gem 'pry-byebug' # byebugをpryで実行 * ブレイクポイントに binding.pry と入れてください。
  gem 'pry-rails' # irbの置き換え。irbより機能豊富。

  # deploy
  gem 'capistrano-rails'

  # document
  gem 'annotate' # Modelにテーブル定義を出力
  gem 'rails-erd', require: false # bundle exec erdでERDを生成
  gem 'redcarpet' # markdown用gem

  # linter
  gem 'guard-rubocop' # ファイル変更時に自動的にrubocopを実行する
  gem 'rubocop'
  gem 'rubocop-checkstyle_formatter'
  gem 'rubocop-rspec'

  # preloader
  gem 'bootsnap', require: false # Railsの起動を高速化
  gem 'spring' # コマンド実行の高速化
  gem 'spring-watcher-listen', '~> 2.0.0'

  # profiler
  gem 'activerecord-cause' # クエリの実行された箇所をログに出力
  gem 'brakeman' # セキュリティチェックを行う
  gem 'bundler-audit' # Gemfile.lockのgemバージョンを調べてセキュリティ問題をチェック
end


# AWSのサポート
gem 'aws-sdk-rails'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]